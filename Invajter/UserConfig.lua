function Invajter.SavedVars()
    if (Invajter.language == "Alliance") then
        Invajter.language = "common"
    else
        Invajter.language = "orcish"
    end
    if  InvajterSV == nil then
        InvajterSV = {}
        InvajterSV["version"] = 0
    end
    if InvajterSV["version"] < 25.1 then
        if InvajterSV["On"] then InvajterSV["on"] = InvajterSV["On"]; InvajterSV["On"] = nil end
        if InvajterSV["Max"] then InvajterSV["max"] = InvajterSV["Max"]; InvajterSV["Max"] = nil end
    end

    if not InvajterSV["on"] then InvajterSV["on"] = 1 end
    if not InvajterSV["max"] then InvajterSV["max"] = 40 end
    if not InvajterSV["cache"] then InvajterSV["cache"] = 1 end
    if not InvajterSV["command"] then InvajterSV["command"] = {"invite", "inv"} end
    if not InvajterSV["blacklist"] then InvajterSV["blacklist"] = {} end
    if not InvajterSV["strings"] then InvajterSV["strings"] = {} end
    if not InvajterSV["strings"]["echo"] then InvajterSV["strings"]["echo"] = "Full, sorry." end
    if not InvajterSV["max"] then InvajterSV["max"] = InvajterSV["Max"] end
    if not InvajterSV["cmd"] then InvajterSV["cmd"] = {} end
    if not InvajterSV["cmd"]["add"] then InvajterSV["cmd"]["add"] = {"add","+"} end
    if not InvajterSV["cmd"]["delete"] then InvajterSV["cmd"]["delete"] = {"delete","del","-"} end
    if not InvajterSV["cmd"]["command"] then InvajterSV["cmd"]["command"] = {"command","cmd"} end
    if not InvajterSV["cmd"]["blacklist"] then InvajterSV["cmd"]["blacklist"] = {"blacklist","bl"} end
    if not InvajterSV["cmd"]["maximum"] then InvajterSV["cmd"]["maximum"] = {"maximum","max"} end
    if not InvajterSV["cmd"]["help"] then InvajterSV["cmd"]["help"] = {"help"} end
    if not InvajterSV["cmd"]["cache"] then InvajterSV["cmd"]["cache"] = {"cache"} end
    if not InvajterSV["cmd"]["on"] then InvajterSV["cmd"]["on"] = {"on"} end
    if not InvajterSV["cmd"]["off"] then InvajterSV["cmd"]["off"] = {"off"} end
    if not InvajterSV["cmd"]["toggle"] then InvajterSV["cmd"]["toggle"] = {"toggle"} end
    if not InvajterSV["cmd"]["echo"] then InvajterSV["cmd"]["echo"] = {"echo"} end
    if not InvajterSV["cmd"]["set"] then InvajterSV["cmd"]["set"] = {"set"} end
    if not InvajterSV["cmd"]["show"] then InvajterSV["cmd"]["show"] = {"show"} end
    if not InvajterSV["cmd"]["alternative"] then InvajterSV["cmd"]["alternative"] = {"alternative","alt"} end
    if not InvajterSV["cmd"]["reload"] then InvajterSV["cmd"]["reload"] = {"reload"} end
    if not InvajterSV["minimap"] then InvajterSV["minimap"] = -45 end
    if not InvajterSV["include_self"] then InvajterSV["include_self"] = 0 end
    
    if InvajterSV["version"] < Invajter.version then
        InvajterSV["version"] = Invajter.version
    end
     Invajter.InitializeUI()
end

function Invajter.HandleEcho(cmd,subcmd)
    if Invajter.cmd_set(cmd) then
        InvajterSV["strings"]["echo"] = subcmd
    elseif Invajter.cmd_toggle(cmd) then
        Invajter.ToggleEcho(InvajterSV["echo"])
    elseif Invajter.cmd_on(cmd) then
        Invajter.ToggleEcho(false)
    elseif Invajter.cmd_off(cmd) then
        Invajter.ToggleEcho(true)
    else
        if (InvajterSV["echo"] == 1) then
            Invajter.Print("|cff20ff20Invajter Echo (auto-reply pri full group)\n|cffff2020_____________|cff601020\n/Invajter "..InvajterSV["cmd"]["echo"][1].." "..InvajterSV["cmd"]["on"][1].." - zapne echo\n/Invajter "..InvajterSV["cmd"]["echo"][1].." "..InvajterSV["cmd"]["off"][1].." - vypne echo\nAktualni echo pri plnem stavu:\n   "..InvajterSV["strings"]["echo"].."\n/Invajter "..InvajterSV["cmd"]["echo"][1].." "..InvajterSV["cmd"]["set"][1].." 'text' - nastavi 'text' jako aktualni echo")
        else
            Invajter.Print("echo je vypnute")
        end
    end
end

function Invajter.ToggleEcho(bool)
    if bool == 1 then
        InvajterSV["echo"] = 0
        Invajter.Print("Echo je |cffff2020Vypnute")
        Invajter_button_echo_toggle:SetText("Echo off")
    else
        InvajterSV["echo"] = 1
        Invajter.Print("Echo je |cffff2020Zapnute")
        Invajter_button_echo_toggle:SetText("Echo on")
    end
end

function Invajter.ChangeMaximum(cmd)
    Invajter_amount=tonumber(cmd)
    if Invajter_amount then
        InvajterSV["max"]=min(Invajter_amount,40)
        Invajter.Print("Invajter: maximalni pocet lidi byl nastaven na: |cffff2020" ..InvajterSV["max"].. "|cff601020.")
        if InvajterSV["max"] >= 5 then
            ConvertToRaid()
        end
    end
end

function Invajter.ToggleIncludeSelf(bool)
    if bool == 1 then
        InvajterSV["include_self"] = 0
        Invajter.Print("Maximalni pocet lidi v parte se pocita bez leadera.")
        Invajter_button_delete:SetText("Include Self: Off")
    else
        InvajterSV["include_self"] = 1
        Invajter.Print("Maximalni pocet lidi v parte se pocita s leaderem.")
        Invajter_button_delete:SetText("Include Self: On")
    end
end

function Invajter.HandleAlternatives(msg)
    local cmd, subcmd = Invajter.GetCommand(msg)
    if Invajter.cmd_add(cmd) then
        Invajter.HandleAlternativesAdd(subcmd)
    elseif Invajter.cmd_delete(cmd) then
        Invajter.HandleAlternativesDelete(subcmd)
    elseif Invajter.cmd_show(cmd) then
        Invajter.ShowAlternatives(subcmd)
    else 
        Invajter.Print("|cff20ff20Invajter alternative\n|cffff2020_____________|cff601020\n/Invajter "..InvajterSV["cmd"]["alternative"][1].." "..InvajterSV["cmd"]["add"][1].." 'a' 'b' - prida ke stavajicimu prikazu 'a' moznou alternativu 'b'\nPriklad: /Invajter "..InvajterSV["cmd"]["alternative"][1].." "..InvajterSV["cmd"]["command"][1].." prikaz -> lze pouzit pak /Invajter prikaz misto /Invajter "..InvajterSV["cmd"]["command"][1].."\n\n/Invajter "..InvajterSV["cmd"]["alternative"][1].." "..InvajterSV["cmd"]["delete"][1].." - 'a' 'b' - smaze alternativni moznost 'b' pro prikaz 'a'\n/Invajter "..InvajterSV["cmd"]["alternative"][1].." "..InvajterSV["cmd"]["show"][1].." 'a' - zobrazi veskera synonyma k prikazu 'a'")
    end
end

function Invajter.HandleAlternativesDelete(msg)
    local cmd, subcmd = Invajter.GetCommand(msg)
    subcmd = Invajter.GetCommand(subcmd)
    Invajter_mod = Invajter.FindAlternatives(cmd)
    if Invajter_mod then
        if not InvajterSV["cmd"][Invajter_mod][2] then
            Invajter.Print("Nelze smazat posledni mozny prikaz pro "..InvajterSV["cmd"][Invajter_mod][1])
            return
        end
        i=1
        while InvajterSV["cmd"][Invajter_mod][i]~=nil and InvajterSV["cmd"][Invajter_mod][i]~=subcmd do
            i=i+1
        end
        Invajter.Print(InvajterSV["cmd"][Invajter_mod][i].." prikaz pro "..InvajterSV["cmd"][Invajter_mod][1].." byl smazan")
        InvajterSV["cmd"][Invajter_mod][i]=nil
        repeat
            i=i+1
            InvajterSV["cmd"][Invajter_mod][i-1]=InvajterSV["cmd"][Invajter_mod][i]
        until InvajterSV["cmd"][Invajter_mod][i+1]==nil
    else
        Invajter.Print("Prikaz "..cmd.." nebyl nalezen.")
    end
end

function Invajter.HandleAlternativesAdd(msg)
    local cmd, subcmd = Invajter.GetCommand(msg)
    subcmd = Invajter.GetCommand(subcmd)
    Invajter_mod = Invajter.FindAlternatives(cmd)
    Invajter_exists = Invajter.FindAlternatives(subcmd)
    if Invajter_exists then
        Invajter.Print("Prikaz "..subcmd" je jiz pouzit jako aktualni prikaz pro"..InvajterSV["cmd"][Invajter_exists][1])
    end
    if Invajter_mod then
        i=0
        while InvajterSV["cmd"][Invajter_mod][i+1] do
            i=i+1
            if InvajterSV["cmd"][Invajter_mod][i] == subcmd then
                return
            end
        end
    InvajterSV["cmd"][Invajter_mod][i+1] = subcmd
    Invajter.Print("Prikaz "..InvajterSV["cmd"][Invajter_mod][i+1].." byl uspesne pridan pro "..InvajterSV["cmd"][cmd][1]..".")
    end
end

function Invajter.ShowAlternatives(cmd)
    Invajter_mod = Invajter.FindAlternatives(cmd)
    if Invajter_mod then
        Invajter.Print("Alternativy pro prikaz "..cmd.." jsou:")
        i=1
        while InvajterSV["cmd"][Invajter_mod][i] do
            Invajter.Print(InvajterSV["cmd"][Invajter_mod][i])
            i=i+1
        end
    else
        Invajter.Print("Prikaz "..cmd.." nebyl nalezen.")
    end
end

function Invajter.Toggle(bool)
    if bool == 1 then
        InvajterSV["on"] = 0
        Invajter.Print("Invajter je |cffff2020Vypnuty")
        Invajter_button_stav:SetText("Invajter Off")
    else
        InvajterSV["on"] = 1
        Invajter.Print("Invajter je |cffff2020Zapnuty")
        Invajter_button_stav:SetText("Invajter On")
    end
end